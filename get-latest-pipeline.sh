#!/bin/bash

CURL="curl --silent"

if [ -z "${KERNEL_REPO}" ]; then
  KERNEL_REPO="https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc.git"
fi
project="${KERNEL_REPO#https://gitlab.com/*}"
project="${project%*.git}"

tag_series="v5.*.*"
if [ -n "${KERNEL_BRANCH}" ]; then
  # KERNEL_BRANCH=linux-5.15.y
  tag_series="v${KERNEL_BRANCH#linux-*}"
  tag_series="${tag_series%*.y}.*"
fi

# Look for most recent stable branch SHA
tags_txt="$(mktemp)"
# shellcheck disable=SC2086
git ls-remote --tags --sort=v:refname \
  "${KERNEL_REPO}" ${tag_series} > "${tags_txt}"
last_revision="$(grep -E '\^\{\}$' "${tags_txt}" | tail -n1)"
sha="$(echo "${last_revision}" | awk '{print $1}')"
export LATEST_SHA="${sha}"
rm "${tags_txt}"

# Get pipeline for latest release from latest stable branch
pipeline_json="$(mktemp)"
project_id="${project//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"
# shellcheck disable=SC2068
${CURL[@]} "${project_url}/pipelines?sha=${sha}" > "${pipeline_json}"
pipeline_id="$(jq -r .[].id "${pipeline_json}")"
ref="$(jq -r .[].ref "${pipeline_json}")"
rm "${pipeline_json}"

export LATEST_RELEASE_PIPELINE_ID="${pipeline_id}"

arg="--pipeline-id"
if [ $# -gt 0 ]; then
  arg="$1"
fi

if [ "${arg}" = "--pipeline-id" ]; then
  echo "${pipeline_id}"
elif [ "${arg}" = "--latest-sha" ]; then
  echo "${sha}"
elif [ "${arg}" = "--kernel-branch" ]; then
  echo "${ref}"
fi
